package com.example.renan.shoppinglist;

import android.graphics.Bitmap;

public class Product {
    private String m_Name;
    private String m_amount;
    private String m_PricePerUint;
    private boolean m_IsCheck;
    private Bitmap m_bitmap;

    public Product(String m_Name, String m_amount, String m_PricePerUint, boolean m_IsCheck, Bitmap m_bitmap) {
        this.m_Name = m_Name;
        this.m_amount = m_amount;
        this.m_PricePerUint = m_PricePerUint;
        this.m_IsCheck = m_IsCheck;
        this.m_bitmap = m_bitmap;
    }


    public Bitmap getM_bitmap() {
        return m_bitmap;
    }

    public void setM_bitmap(Bitmap m_bitmap) {
        this.m_bitmap = m_bitmap;
    }

    public String getM_Name() {
        return m_Name;
    }

    public void setM_Name(String m_Name) {
        this.m_Name = m_Name;
    }

    public String getM_amount() {
        return m_amount;
    }

    public void setM_amount(String m_amount) {
        this.m_amount = m_amount;
    }

    public String getM_PricePerUint() {
        return m_PricePerUint;
    }

    public void setM_PricePerUint(String m_PricePerUint) {
        this.m_PricePerUint = m_PricePerUint;
    }

    public boolean isM_IsCheck() {
        return m_IsCheck;
    }

    public void setM_IsCheck(boolean m_IsCheck) {
        this.m_IsCheck = m_IsCheck;
    }
}

