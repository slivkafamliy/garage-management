package com.whodrive.renan.whodrive;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class GoingOutAdapter extends RecyclerView.Adapter<GoingOutAdapter.GoingOutViewHolder>{
    private List<GoingOut> mGoingOutMeetings;
    private  MyGoingOutListenerIO listener;

    interface MyGoingOutListenerIO
    {
        void onItemClicked(int position, View view);
        void onLongItemClicked(int position, View view);
    }

    public void setListener(MyGoingOutListenerIO listener) {
        this.listener = listener;
    }

    public GoingOutAdapter(List<GoingOut> mGoingOutMeetings) {
        this.mGoingOutMeetings = mGoingOutMeetings;
    }

    public class GoingOutViewHolder extends RecyclerView.ViewHolder
    {
        TextView driversNameListTv;
        TextView numberOfMembersIntTv;
        TextView numberOfCarsIntTv;
        TextView dateTv;
        ImageView camreaIb;

        public GoingOutViewHolder( View itemView) {
            super(itemView);

            driversNameListTv = itemView.findViewById(R.id.drivers_name_List_Tv);
            numberOfMembersIntTv= itemView.findViewById(R.id.member_in_meeting_int_Tv);
            numberOfCarsIntTv= itemView.findViewById(R.id.number_of_cars_int_Tv);
            dateTv = itemView.findViewById(R.id.date_tv);
            camreaIb = itemView.findViewById(R.id.photo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                    {
                        listener.onItemClicked(getAdapterPosition(),v);
                    }
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(listener != null)
                    {
                        listener.onLongItemClicked(getAdapterPosition(),v);
                    }
                    return false;
                }
            });
        }
    }


    @Override
    public GoingOutViewHolder onCreateViewHolder( ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.going_out_item,viewGroup,false);
        GoingOutViewHolder holder = new GoingOutViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder( GoingOutViewHolder goingOutViewHolder, int position) {
        GoingOut goingOut = mGoingOutMeetings.get(position);

        goingOutViewHolder.driversNameListTv.setText(goingOut.getmDriversName());
        goingOutViewHolder.numberOfMembersIntTv.setText(goingOut.getmMembersMeeting());
        goingOutViewHolder.numberOfCarsIntTv.setText(goingOut.getmNumberOfCars()+"");
        goingOutViewHolder.dateTv.setText(goingOut.getmDate());
        goingOutViewHolder.camreaIb.setImageBitmap(goingOut.getmPicture());
    }

    @Override
    public int getItemCount() {
        return mGoingOutMeetings.size();
    }
}
