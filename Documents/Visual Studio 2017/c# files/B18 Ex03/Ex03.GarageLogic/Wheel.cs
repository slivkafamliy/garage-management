﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.GarageLogic
{
    public class Wheel
    {
        public string m_ManufacturerName;
        public float m_CurrentAirPressure;
        public float m_MaxAirPressure;
       
        public Wheel(string ManufacturerName, float CurrentAirPressure, float MaxAirPressure)
        {
            this.m_ManufacturerName = ManufacturerName;
            this.m_CurrentAirPressure = CurrentAirPressure;
            this.m_MaxAirPressure = MaxAirPressure;
        }
        public static void Inflate(float CurrentAirPressure,float MaxAirPressure)
        {
            if( MaxAirPressure > CurrentAirPressure )
            {
                CurrentAirPressure = MaxAirPressure;
            }
        }


    }
}
