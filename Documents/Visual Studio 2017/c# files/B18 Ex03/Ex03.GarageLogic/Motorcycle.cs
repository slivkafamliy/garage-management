﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex03.GarageLogic
{
    public abstract class Motorcycle : Vehicle
    {
        public List<string> m_LicenseTypeAllowedToDrive = new List<string>() { "A", "A1", "B1", "B2" };
        
        public Motorcycle(List<string> LicenseTypeAllowedToDrive,
            string ModelName, string LicenseNumber, int numbersOfWheels, 
            string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, 
            FuelTypes m_FuelType, float CapacityOfCargo, float CurrAmountTank, ConditionInTheGarage i_ConditionInTheGarage,string m_OwnersName, string m_PhoneName)
            : base(ModelName, LicenseNumber, numbersOfWheels, m_ManufacturerName, m_CurrentAirPressure, 
                  m_MaxAirPressure, m_FuelType, CapacityOfCargo, CurrAmountTank, i_ConditionInTheGarage, m_OwnersName, m_PhoneName)
        {
            this.m_LicenseTypeAllowedToDrive = LicenseTypeAllowedToDrive;
        }

    }
}
