﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ex03.GarageLogic
{
    public class Garage
    {
        public List<Vehicle> VehicleInGarage = new List<Vehicle>();

        public bool CreateFuelTruckToAdd(bool IsTheTrunkCooled, string ModelName, string LicenseNumber, int numbersOfWheels,
               string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, float CurrAmountTank, string m_OwnersName, string i_PhoneName)
        {
            FuelTruck a1 = new FuelTruck(IsTheTrunkCooled, ModelName, LicenseNumber, 12,
                m_ManufacturerName, m_CurrentAirPressure, 28, CurrAmountTank, ConditionInTheGarage.InRepair, m_OwnersName, i_PhoneName);

            return IsTheVehicleAlreadyInGarage(a1);
        }

        public bool CreateFuelCarToAdd(NumOfDoors numOfDoors, Color color, string ModelName, string LicenseNumber,
            int numbersOfWheels, string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, float CurrAmountTank, string m_OwnersName, string i_PhoneName)
        {
            FuelCar a1 = new FuelCar(numOfDoors, color, ModelName, LicenseNumber, 4,
                m_ManufacturerName, m_CurrentAirPressure, 32, CurrAmountTank, ConditionInTheGarage.InRepair, m_OwnersName, i_PhoneName);

            return IsTheVehicleAlreadyInGarage(a1);
        }

        public bool CreateElectricCarToAdd(NumOfDoors numOfDoors, Color color, string ModelName, string LicenseNumber,
            int numbersOfWheels, string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, float CurrAmountTank, string m_OwnersName, string i_PhoneName)
        {
            ElectricCar a1 = new ElectricCar(numOfDoors, color, ModelName, LicenseNumber, 4,
                m_ManufacturerName, m_CurrentAirPressure, 32, CurrAmountTank, ConditionInTheGarage.InRepair, m_OwnersName, i_PhoneName);

            return IsTheVehicleAlreadyInGarage(a1);
        }

        public bool CreateFuelMotorcycleToAdd(List<string> LicenseTypeAllowedToDrive, string ModelName, string LicenseNumber,
                int numbersOfWheels, string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, float CurrAmountTank, string m_OwnersName, string i_PhoneName)
        {
            FuelMotorcycle a1 = new FuelMotorcycle(LicenseTypeAllowedToDrive, ModelName, LicenseNumber, 2,
                m_ManufacturerName, m_CurrentAirPressure, 30, CurrAmountTank, ConditionInTheGarage.InRepair, m_OwnersName, i_PhoneName);

            return IsTheVehicleAlreadyInGarage(a1);
        }

        public bool CreateElectricMotorcycleToAdd(List<string> LicenseTypeAllowedToDrive, string ModelName, string LicenseNumber,
            int numbersOfWheels, string m_ManufacturerName, float m_CurrentAirPressure, float m_MaxAirPressure, float CurrAmountTank, string m_OwnersName, string i_PhoneName)
        {
            ElectricMotorcycle a1 = new ElectricMotorcycle(LicenseTypeAllowedToDrive, ModelName, LicenseNumber, 2,
                m_ManufacturerName, m_CurrentAirPressure, 30, CurrAmountTank, ConditionInTheGarage.InRepair, m_OwnersName, i_PhoneName);
            return IsTheVehicleAlreadyInGarage(a1);
        }

        public bool IsTheVehicleAlreadyInGarage(Vehicle a1)
        {
            bool isTheVehicleAlreadyInGarage = false;
            if (VehicleInGarage.Capacity == 0)
            {
                VehicleInGarage.Add(a1);
            }
            else
            {
                foreach (Vehicle vehicle in VehicleInGarage)
                {
                    if (!vehicle.m_LicenseNumber.Contains(a1.m_LicenseNumber))
                    {
                        VehicleInGarage.Add(a1);
                        isTheVehicleAlreadyInGarage = false;
                        break;
                    }
                    else
                    {
                        isTheVehicleAlreadyInGarage = true;
                    }
                }

            }
            return isTheVehicleAlreadyInGarage;
        }

    }
}


