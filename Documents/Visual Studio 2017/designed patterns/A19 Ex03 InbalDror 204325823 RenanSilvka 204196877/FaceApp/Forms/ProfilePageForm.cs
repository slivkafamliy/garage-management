﻿using System;
using System.Windows.Forms;
using A16_Ex01_Tom_302986575_Bar_200321412;
using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public partial class ProfilePageForm : Form
    {
        private readonly UserFacebook r_UserFacebook;
        private WindowSettings m_WindowSettings;
        public IBridge Bridge;

        public ProfilePageForm()
        {
            InitializeComponent();
            this.r_UserFacebook = UserFacebook.getInstance();
            this.m_WindowSettings = WindowSettings.getInstance();
            m_WindowSettings.setProfilePage(this);                        
        }

      
        private void ProfilePage_Load(object sender, EventArgs e)
        {
            performGenderRelatedActions();

            this.Text = "Logged in as " + this.r_UserFacebook.LoggedInUser.FirstName + "  " + this.r_UserFacebook.LoggedInUser.LastName;
            circularPictureBoxProfile.ImageLocation = this.r_UserFacebook.LoggedInUser.PictureNormalURL;
            if (r_UserFacebook.LoggedInUser.Cover != null)
            {
                pictureBoxCover.ImageLocation = r_UserFacebook.LoggedInUser.Cover.SourceURL;
            }                    
        }

        private void performGenderRelatedActions()
        {
            if (r_UserFacebook.LoggedInUser.Gender.ToString() == "male")
            {
                MaleUserVisitor maleUser = new MaleUserVisitor(this.heroCircularPictureBox);
                maleUser.visit();
                Bridge = new ManColor();
                Bridge.ChangeBackColor(this);
            }
            else
            {
                FemaleUserVisitor femaleUser = new FemaleUserVisitor(this.heroCircularPictureBox);
                femaleUser.visit();
                Bridge = new WomanColor();
                Bridge.ChangeBackColor(this);
            }
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            r_UserFacebook.Logout(this);
        }

        private void buttonShowAlbum_Click(object sender, EventArgs e)
        {
            this.Hide();
            Type type = typeof(AlbumForm);
            Form form = FormFactory.CreateForm(type);
            form.ShowDialog();
            this.Close();
        }

        private void buttonShowPosts_Click(object sender, EventArgs e)
        {
            this.Hide();
            Type type = typeof(PostForm);
            Form form = FormFactory.CreateForm(type);
            form.ShowDialog();
            this.Close();
        }

        private void buttonShowCheckins_Click(object sender, EventArgs e)
        {
            this.Hide();
            Type type = typeof(CheckinsPage);
            Form form = FormFactory.CreateForm(type);
            form.ShowDialog();
            this.Close();
        }

        private void buttonShowFriends_Click(object sender, EventArgs e)
        {
            this.Hide();
            Type type = typeof(FriendsForm);
            Form form = FormFactory.CreateForm(type);
            form.ShowDialog();
            this.Close();
        }
    }
}