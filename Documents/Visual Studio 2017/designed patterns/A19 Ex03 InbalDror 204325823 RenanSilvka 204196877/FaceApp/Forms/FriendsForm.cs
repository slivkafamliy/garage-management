﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using FacebookWrapper.ObjectModel;

namespace FaceApp
{
    public partial class FriendsForm : Form
    {
        private readonly UserFacebook r_UserFacebook;
        private WindowSettings m_WindowSettings;
        private Carousel m_Carousel;
        private List<User> m_Items;
        public IBridge Bridge;

        public FriendsForm()
        {
            InitializeComponent();
            this.r_UserFacebook = UserFacebook.getInstance();
            this.m_WindowSettings = WindowSettings.getInstance();
            this.m_Items = new List<User>();
            m_WindowSettings.SetFriendsPage(this);
        }

        private List<CarouselItem> prepareItems(List<User> list)
        {
            List<CarouselItem> c_items = new List<CarouselItem>();
            foreach (User item in list)
            {
                CarouselItem c_item = new CarouselItem(item.Name, item.LastName, item.PictureLargeURL);
                c_items.Add(c_item);
            }

            return c_items;
        }

        private void FreindsPage_Load(object sender, EventArgs e)
        {
            if (r_UserFacebook.LoggedInUser.Gender.ToString() == "male")
            {
                Bridge = new ManColor();
                Bridge.ChangeBackColor(this);
            }
            else
            {
                Bridge = new WomanColor();
                Bridge.ChangeBackColor(this);
            }

            new Thread(fetchFriends).Start();                     
        }

        private void buttonForward_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayForward();
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            m_Carousel.DisplayPrevious();
        }

        private void fetchFriends()
        {
            m_Items.Clear();
            foreach (User friend in this.r_UserFacebook.LoggedInUser.Friends)
            {
                Invoke((MethodInvoker)(() =>
                    m_Items.Add(friend)
                ));              
            }

            if (this.r_UserFacebook.LoggedInUser.Friends.Count == 0)
            {
                MessageBox.Show("No Friends to retrieve :(");
            }
            else
            {
                m_Carousel = new Carousel(this.pictureBox, this.labelName, this.prepareItems(this.m_Items));
            }
        }

        private void buttonBackToProfilePage_Click(object sender, EventArgs e)
        {
            this.Hide();
            ProfilePageForm window = new ProfilePageForm();
            window.ShowDialog();
            this.Close();
        }
    }
}
