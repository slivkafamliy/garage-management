﻿namespace FaceApp
{
    public partial class AlbumForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlbumForm));
            this.labelName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonBackToProfilePage = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonForward = new System.Windows.Forms.Button();
            this.labelNumOfLikes = new System.Windows.Forms.Label();
            this.HowManyTagesBar = new System.Windows.Forms.TrackBar();
            this.buttonFavoritePic = new System.Windows.Forms.Button();
            this.pictureBoxFavPic2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFavPic4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFavPic3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFavPic1 = new System.Windows.Forms.PictureBox();
            this.labelExplain = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowManyTagesBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelName.Location = new System.Drawing.Point(202, 28);
            this.labelName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(51, 20);
            this.labelName.TabIndex = 37;
            this.labelName.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(37, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 36;
            this.label1.Text = "Album Name:";
            // 
            // buttonBackToProfilePage
            // 
            this.buttonBackToProfilePage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBackToProfilePage.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.buttonBackToProfilePage.Image = global::FaceApp.Properties.Resources.iconfinder_Undo_27885;
            this.buttonBackToProfilePage.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.buttonBackToProfilePage.Location = new System.Drawing.Point(1173, 13);
            this.buttonBackToProfilePage.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBackToProfilePage.Name = "buttonBackToProfilePage";
            this.buttonBackToProfilePage.Size = new System.Drawing.Size(52, 31);
            this.buttonBackToProfilePage.TabIndex = 39;
            this.buttonBackToProfilePage.Text = "back";
            this.buttonBackToProfilePage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonBackToProfilePage.UseVisualStyleBackColor = true;
            this.buttonBackToProfilePage.Click += new System.EventHandler(this.buttonBackToProfilePage_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.AutoSize = true;
            this.buttonBack.Image = global::FaceApp.Properties.Resources.back_23;
            this.buttonBack.Location = new System.Drawing.Point(13, 313);
            this.buttonBack.Margin = new System.Windows.Forms.Padding(4);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(126, 54);
            this.buttonBack.TabIndex = 38;
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox.Location = new System.Drawing.Point(13, 73);
            this.pictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(294, 231);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox.TabIndex = 35;
            this.pictureBox.TabStop = false;
            // 
            // buttonForward
            // 
            this.buttonForward.AutoSize = true;
            this.buttonForward.Image = global::FaceApp.Properties.Resources.forward_2;
            this.buttonForward.Location = new System.Drawing.Point(179, 313);
            this.buttonForward.Margin = new System.Windows.Forms.Padding(4);
            this.buttonForward.Name = "buttonForward";
            this.buttonForward.Size = new System.Drawing.Size(126, 54);
            this.buttonForward.TabIndex = 33;
            this.buttonForward.UseVisualStyleBackColor = true;
            this.buttonForward.Click += new System.EventHandler(this.buttonForward_Click);
            // 
            // labelNumOfLikes
            // 
            this.labelNumOfLikes.AutoSize = true;
            this.labelNumOfLikes.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.labelNumOfLikes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelNumOfLikes.ForeColor = System.Drawing.Color.Black;
            this.labelNumOfLikes.Location = new System.Drawing.Point(793, 64);
            this.labelNumOfLikes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelNumOfLikes.Name = "labelNumOfLikes";
            this.labelNumOfLikes.Size = new System.Drawing.Size(19, 20);
            this.labelNumOfLikes.TabIndex = 64;
            this.labelNumOfLikes.Text = "0";
            // 
            // HowManyTagesBar
            // 
            this.HowManyTagesBar.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.HowManyTagesBar.Location = new System.Drawing.Point(666, 24);
            this.HowManyTagesBar.Name = "HowManyTagesBar";
            this.HowManyTagesBar.Size = new System.Drawing.Size(256, 45);
            this.HowManyTagesBar.TabIndex = 63;
            this.HowManyTagesBar.Scroll += new System.EventHandler(this.HowManyTagesBar_Scroll);
            // 
            // buttonFavoritePic
            // 
            this.buttonFavoritePic.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonFavoritePic.Font = new System.Drawing.Font("Aharoni", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.buttonFavoritePic.ForeColor = System.Drawing.Color.Black;
            this.buttonFavoritePic.Location = new System.Drawing.Point(936, 24);
            this.buttonFavoritePic.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.buttonFavoritePic.Name = "buttonFavoritePic";
            this.buttonFavoritePic.Size = new System.Drawing.Size(204, 66);
            this.buttonFavoritePic.TabIndex = 62;
            this.buttonFavoritePic.Text = "Find My Pictures";
            this.buttonFavoritePic.UseMnemonic = false;
            this.buttonFavoritePic.UseVisualStyleBackColor = false;
            this.buttonFavoritePic.Click += new System.EventHandler(this.buttonFavoritePic_Click);
            // 
            // pictureBoxFavPic2
            // 
            this.pictureBoxFavPic2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxFavPic2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxFavPic2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFavPic2.Image")));
            this.pictureBoxFavPic2.Location = new System.Drawing.Point(550, 210);
            this.pictureBoxFavPic2.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.pictureBoxFavPic2.Name = "pictureBoxFavPic2";
            this.pictureBoxFavPic2.Size = new System.Drawing.Size(114, 94);
            this.pictureBoxFavPic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFavPic2.TabIndex = 61;
            this.pictureBoxFavPic2.TabStop = false;
            this.pictureBoxFavPic2.UseWaitCursor = true;
            // 
            // pictureBoxFavPic4
            // 
            this.pictureBoxFavPic4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxFavPic4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxFavPic4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFavPic4.Image")));
            this.pictureBoxFavPic4.Location = new System.Drawing.Point(879, 210);
            this.pictureBoxFavPic4.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.pictureBoxFavPic4.Name = "pictureBoxFavPic4";
            this.pictureBoxFavPic4.Size = new System.Drawing.Size(112, 94);
            this.pictureBoxFavPic4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFavPic4.TabIndex = 60;
            this.pictureBoxFavPic4.TabStop = false;
            this.pictureBoxFavPic4.UseWaitCursor = true;
            // 
            // pictureBoxFavPic3
            // 
            this.pictureBoxFavPic3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxFavPic3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxFavPic3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFavPic3.Image")));
            this.pictureBoxFavPic3.Location = new System.Drawing.Point(718, 119);
            this.pictureBoxFavPic3.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.pictureBoxFavPic3.Name = "pictureBoxFavPic3";
            this.pictureBoxFavPic3.Size = new System.Drawing.Size(113, 95);
            this.pictureBoxFavPic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFavPic3.TabIndex = 59;
            this.pictureBoxFavPic3.TabStop = false;
            this.pictureBoxFavPic3.UseWaitCursor = true;
            // 
            // pictureBoxFavPic1
            // 
            this.pictureBoxFavPic1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxFavPic1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBoxFavPic1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFavPic1.Image")));
            this.pictureBoxFavPic1.Location = new System.Drawing.Point(368, 119);
            this.pictureBoxFavPic1.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.pictureBoxFavPic1.Name = "pictureBoxFavPic1";
            this.pictureBoxFavPic1.Size = new System.Drawing.Size(116, 95);
            this.pictureBoxFavPic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFavPic1.TabIndex = 58;
            this.pictureBoxFavPic1.TabStop = false;
            this.pictureBoxFavPic1.UseWaitCursor = true;
            // 
            // labelExplain
            // 
            this.labelExplain.AutoSize = true;
            this.labelExplain.Font = new System.Drawing.Font("Aharoni", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelExplain.Location = new System.Drawing.Point(418, 36);
            this.labelExplain.Name = "labelExplain";
            this.labelExplain.Size = new System.Drawing.Size(175, 14);
            this.labelExplain.TabIndex = 65;
            this.labelExplain.Text = "Choose number of Tages:";
            // 
            // AlbumPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1238, 379);
            this.Controls.Add(this.labelExplain);
            this.Controls.Add(this.labelNumOfLikes);
            this.Controls.Add(this.HowManyTagesBar);
            this.Controls.Add(this.buttonFavoritePic);
            this.Controls.Add(this.pictureBoxFavPic2);
            this.Controls.Add(this.pictureBoxFavPic4);
            this.Controls.Add(this.pictureBoxFavPic3);
            this.Controls.Add(this.pictureBoxFavPic1);
            this.Controls.Add(this.buttonBackToProfilePage);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.buttonForward);
            this.Font = new System.Drawing.Font("Rage Italic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AlbumPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AlbumPages";
            this.Load += new System.EventHandler(this.AlbumPages_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HowManyTagesBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFavPic1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button buttonForward;
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.Button buttonBackToProfilePage;
        private System.Windows.Forms.Label labelNumOfLikes;
        private System.Windows.Forms.TrackBar HowManyTagesBar;
        private System.Windows.Forms.Button buttonFavoritePic;
        private System.Windows.Forms.PictureBox pictureBoxFavPic2;
        private System.Windows.Forms.PictureBox pictureBoxFavPic4;
        private System.Windows.Forms.PictureBox pictureBoxFavPic3;
        private System.Windows.Forms.PictureBox pictureBoxFavPic1;
        private System.Windows.Forms.Label labelExplain;
    }
}