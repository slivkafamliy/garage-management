﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using FacebookWrapper;
using FaceApp.Properties;
using A16_Ex01_Tom_302986575_Bar_200321412;
using FaceApp.Strategy_Class;

namespace FaceApp
{
    public partial class LoginForm : Form
    {
        private readonly UserFacebook r_UserFacebook;
        private WindowSettings r_WindowSettings;

        public LoginForm()
        {
            InitializeComponent();
            r_UserFacebook = UserFacebook.getInstance();
            this.r_WindowSettings = WindowSettings.getInstance();
            r_WindowSettings.setLoginPage(this);
            this.Size = new Size(437, 228);
            linkLabel1.DataBindings.Add(new Binding("Text", textBox1, "Text", true, DataSourceUpdateMode.OnPropertyChanged));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Settings.Default.LastAccessToken.Length > 0 && Settings.Default.LastAccessToken != "null")
            {
                r_UserFacebook.LoginResult = FacebookService.Connect(Settings.Default.LastAccessToken);
                r_WindowSettings.setAccessToken(r_UserFacebook.LoginResult.AccessToken);
                createProfilePage();
            }

            this.Location = new Point(Settings.Default.LastLoginPageLocationX, Settings.Default.LastLoginPageLocationY);            
            this.Size = new Size(Settings.Default.LastLoginPageSizeWidth, Settings.Default.LastLoginPageSizeHeight);
            this.CheckBoxRememberMe.Checked = Settings.Default.checkBoxRememberMeLoginPage;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            r_WindowSettings.RememberSettingBeforeClosingWindow(this);
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            if (Settings.Default.checkBoxRememberMeLoginPage && !string.IsNullOrEmpty(Settings.Default.LastAccessToken))
            {
                r_UserFacebook.LoginResult = FacebookService.Connect(Settings.Default.LastAccessToken);
                createProfilePage();
            }
        }

        protected void buttonLogin_Click(object sender, EventArgs e)
        {
            r_UserFacebook.LoginToFacbook();
            createProfilePage();
        }

        private void createProfilePage()
        {
            this.Hide();
            Type type = typeof(ProfilePageForm);
            Form form = FormFactory.CreateForm(type);            

            int age = AgeCalculator.getAge();
            if (age < 18)
            {
                Welcomer wlc = new Welcomer(new ChildWelcomeStrategy());
                wlc.SayHi(age);
            }
            else
            {
                Welcomer wlc = new Welcomer(new AdultWelcomeStrategy());
                wlc.SayHi(age);
            }

            form.ShowDialog();
            this.Close();
        }

        

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://www.facebook.com/" + linkLabel1.Text);
        }
    }
}
