﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaceApp.Strategy_Class
{
    public class ChildWelcomeStrategy : IWelcomeStrategy
    {
        public void SayHi(int age)
        {
         MessageBox.Show("Hi you are child, your age is " + age.ToString() + " and its below 18" );
        }
    }
}
